import React from 'react'
import {BsLinkedin} from 'react-icons/bs'
import {FaGitlab} from 'react-icons/fa'
import {BsInstagram} from 'react-icons/bs'

const HeaderSocials = () => {
  return (
    <div className='header__socials'>
        <a href='https://www.linkedin.com/in/robert-bob-mason/' className='hs-linkedin'><BsLinkedin/></a>
        <a href='https://gitlab.com/bob.j.mason/resume/-/tree/main/' className='hs-gitlab'><FaGitlab/></a>
        <a href='https://www.instagram.com/bobjmason/' className='hs-insta'><BsInstagram/></a>
    </div>
  )
}

export default HeaderSocials