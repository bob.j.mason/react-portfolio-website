import React from 'react'
import Res from '../../assets/Robert_Mason_Resume.pdf'

const Cta = () => {
  return (
    <div className='cta'>
        <a href={ Res } download className='btn'>Download resume</a>
        <a href="#contact" className='btn btn-primary'>Let's talk</a>
    </div>
  )
}

export default Cta