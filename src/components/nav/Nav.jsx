import React from 'react'
import './nav.css'
import {GoHome} from 'react-icons/go'
import {SiAboutdotme} from 'react-icons/si'
import {MdWeb} from 'react-icons/md'
import {MdDesignServices} from 'react-icons/md'
import {RiContactsLine} from 'react-icons/ri'
import { useState } from 'react'

const Services = () => {
  const [activeNav, setActiveNav] = useState('#')
  return (
    <nav>
      <a href="#" onClick={() => setActiveNav('#')} className={activeNav === '#' ? 'active' : ''}><GoHome/></a>
      <a href="#about" onClick={() => setActiveNav('#about')} className={activeNav === '#about' ? 'active' : ''}><SiAboutdotme/></a>
      <a href="#experience" onClick={() => setActiveNav('#experience')} className={activeNav === '#experience' ? 'active' : ''}><MdWeb/></a>
      <a href="#services" onClick={() => setActiveNav('#services')} className={activeNav === '#services' ? 'active' : ''}><MdDesignServices/></a>
      <a href="#contact" onClick={() => setActiveNav('#contact')} className={activeNav === '#contact' ? 'active' : ''}><RiContactsLine/></a>
    </nav>
  )
}

export default Services